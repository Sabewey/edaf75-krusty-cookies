PRAGMA foreign_keys = OFF;
DROP TABLE IF EXISTS pallets;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS cookies;
DROP TABLE IF EXISTS material_amount;
DROP TABLE IF EXISTS raw_materials;
DROP TRIGGER IF EXISTS check_ingredient_availability;
PRAGMA foreign_keys = ON;
 
CREATE TABLE pallets (
    pallet_id       TEXT     DEFAULT (lower(hex(randomblob(16)))),
    blocked         BOOLEAN     DEFAULT FALSE,
    production_date DATE        DEFAULT (DATE()),
    cookie          TEXT,
    PRIMARY KEY (pallet_id)
    FOREIGN KEY (cookie) REFERENCES cookies(name)
);

CREATE TRIGGER check_ingredient_availability

BEFORE INSERT ON pallets
BEGIN
    UPDATE raw_materials
    SET amount = amount - 
    COALESCE((SELECT material_amount.amount 
     FROM material_amount
     WHERE cookie_name = NEW.cookie AND raw_materials.ingredient = material_amount.material), 0) * 54;
    
    SELECT
        CASE WHEN 
            (SELECT min(amount) FROM raw_materials) < 0
        THEN
            RAISE(ROLLBACK, "Not enough ingredients")
        END;
END;

CREATE TABLE customers (
    name   TEXT,
    address         TEXT,
    PRIMARY KEY (name)
);

CREATE TABLE cookies (
    name              TEXT,
    PRIMARY KEY (name)
);

CREATE TABLE material_amount (
    cookie_name     TEXT,
    material        TEXT,
    amount          INTEGER,
    PRIMARY KEY (cookie_name, material)
    FOREIGN KEY (cookie_name) REFERENCES cookies(name)
    FOREIGN KEY (material) REFERENCES raw_materials(ingredient)
);

CREATE TABLE raw_materials (
    ingredient          TEXT,
    amount              INTEGER DEFAULT 0,
    unit                TEXT,
    last_delivered_date DATETIME,
    delivered_amount    INTEGER,
    PRIMARY KEY (ingredient)
)