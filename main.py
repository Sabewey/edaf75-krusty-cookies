from bottle import get, post, run, request, response
import sqlite3
from urllib.parse import quote, unquote

# Create database with schema: cat create-schema.sql | sqlite3 krusty.sqlite
db = sqlite3.connect("krusty.sqlite")


@post('/reset')
def reset():
    query = """
        PRAGMA foreign_keys = OFF;
        DELETE FROM pallets;
        DELETE FROM customers;
        DELETE FROM cookies;
        DELETE FROM material_amount;
        DELETE FROM raw_materials;
        PRAGMA foreign_keys = ON;
        """
    c = db.cursor()
    c.executescript(query)
    response.status = 205
    return {"location": "/"}


"""
Customers
"""


@get('/customers')
def get_movies():
    query = """
        SELECT   name, address
        FROM     customers
        """
    c = db.cursor()
    c.execute(query)
    found = [{"name": name, "address": address}
             for name, address in c]
    response.status = 200
    return {"data": found}


@post('/customers')
def post_customer():
    customer = request.json
    c = db.cursor()
    c.execute(
        """
        INSERT
        INTO       customers(name, address)
        VALUES     (?, ?)
        RETURNING  name
        """,
        [customer['name'], customer['address']]
    )
    found = c.fetchone()
    if not found:
        response.status = 400
        return ""
    else:
        db.commit()
        response.status = 201
        name = found[0]
        return {"location": "/customers/{}".format(quote(name))}


"""
Ingredients
"""


@post('/ingredients')
def post_ingredients():
    ingredient = request.json
    c = db.cursor()
    c.execute(
        """
        INSERT
        INTO       raw_materials(ingredient, unit)
        VALUES     (?, ?)
        RETURNING  ingredient
        """,
        [ingredient['ingredient'], ingredient['unit']]
    )
    found = c.fetchone()
    if not found:
        response.status = 400
        return ""
    else:
        db.commit()
        response.status = 201
        ingredient = found[0]
        return {"location": "/ingredients/{}".format(quote(ingredient))}


@post('/ingredients/<ingredient>/deliveries')
def post_ingredients(ingredient):
    ingredient = unquote(ingredient)
    delivery = request.json
    c = db.cursor()
    c.execute(
        """
        UPDATE  raw_materials
        SET     amount = (
                    SELECT  amount
                    FROM    raw_materials
                    WHERE   ingredient = ?
                    ) + ?,
                last_delivered_date = ?
        WHERE   ingredient = ?
        """,
        [ingredient, delivery['quantity'],
            delivery['deliveryTime'], ingredient]
    )
    db.commit()
    c.execute(
        """
        SELECT  ingredient, amount, unit
        FROM    raw_materials
        WHERE   ingredient = ?
        """,
        [ingredient]
    )
    found = [{"ingredient": ingredient, "amount": amount, "unit": unit}
             for ingredient, amount, unit in c]
    response.status = 201
    return {"data": found}


@get('/ingredients')
def get_ingredients():
    query = """
        SELECT   ingredient, amount, unit
        FROM     raw_materials
        """
    c = db.cursor()
    c.execute(query)
    found = [{"ingredient": ingredient, "quantity": amount, "unit": unit}
             for ingredient, amount, unit in c]
    response.status = 200
    return {"data": found}


"""
Cookies
"""


@post('/cookies')
def post_cookies():
    cookie = request.json
    cookie_name = cookie['name']
    cookie_recipe = cookie['recipe']

    c = db.cursor()
    c.execute(
        """
        INSERT
        INTO       cookies(name)
        VALUES     (?)
        RETURNING  name
        """,
        [cookie_name]
    )
    found = c.fetchone()
    db.commit()

    for ingredient in cookie_recipe:
        c.execute(
            """
            INSERT
            INTO       material_amount(cookie_name, material, amount)
            VALUES     (?, ?, ?)
            """,
            [cookie_name, ingredient['ingredient'], ingredient['amount']]
        )

    if not found:
        response.status = 422
        return ""
    else:
        db.commit()
        response.status = 201
        cookie = found[0]
        return {"location": "/cookies/{}".format(quote(cookie))}

@get('/cookies')
def get_cookies():
    query = """
        SELECT   *
        FROM     cookies
        """
    c = db.cursor()
    cookies = c.execute(query)
    cookies = cookies.fetchall()
    
    data = []
    for cookie in cookies:
        cookie_name = cookie[0]
        nbr = c.execute(
            """
            SELECT  coalesce(COUNT(pallets.cookie), 0)
            FROM    cookies
            JOIN    pallets ON pallets.cookie = cookies.name
            WHERE   pallets.cookie = ?
            AND     pallets.blocked = 0
            """,
            [cookie_name]
        )
        data.append({"name": cookie_name, "pallets": nbr.fetchone()[0]})
    response.status = 200
    return {"data": data}

@get('/cookies/<cookie_name>/recipe')
def get_cookies(cookie_name):
    query = """
        SELECT   material_amount.material, material_amount.amount, raw_materials.unit
        FROM     material_amount
        JOIN     raw_materials ON material_amount.material = raw_materials.ingredient
        WHERE    material_amount.cookie_name = ?
        """
    c = db.cursor()
    c.execute(query, [cookie_name])
    found = c.fetchall()
    if found == None:
        response.status = 404
        return {"data": []}
    else:
        found = [{"ingredient": item[0], "amount": item[1], "unit": item[2]}
                 for item in found]
        response.status = 200
        return {"data": found}


"""
Pallets
"""


@post('/pallets')
def post_pallets():
    pallet = request.json
    cookie = pallet['cookie']
    c = db.cursor()
    try:
        c.execute(
            """
            INSERT INTO pallets (cookie)
            VALUES     (?)
            RETURNING  pallet_id
            """,
            [cookie]
        )
        found = c.fetchone()[0]
        db.commit()
        response.status = 201
        pallet = found
        return {"location": "/pallets/{}".format(quote(pallet))}
    except:
        response.status = 422
        return {"location": ""}


@get('/pallets')
def get_pallets():
    query = """
        SELECT pallet_id, cookie, production_date, blocked
        FROM pallets
        WHERE 1 = 1
        """
    params = []
    if request.query.cookie_name:
        query += " AND cookie = ?"
        params.append(request.query.cookie_name)
    if request.query.after:
        query += " AND production_date < ?"
        params.append(unquote(request.query.after))
    if request.query.before:
        query += " AND production_date > ?"
        params.append(unquote(request.query.before))

    c = db.cursor()
    c.execute(
        query,
        params
    )
    found = [{"id": pallet_id, "cookie": cookie, "productionDate": production_date, "blocked": blocked}
             for pallet_id, cookie, production_date, blocked in c]
    response.status = 200
    return {"data": found}


@post('/cookies/<cookie_name>/block')
def post_block(cookie_name):
    query = """
        UPDATE pallets
        SET blocked = 1
        WHERE    1 = 1
        """
    params = []
    query += " AND cookie = ?"
    params.append(cookie_name)
    if request.query.after:
        query += " AND production_date < ?"
        params.append(unquote(request.query.after))
    if request.query.before:
        query += " AND production_date > ?"
        params.append(unquote(request.query.before))

    c = db.cursor()
    c.execute(
        query,
        params
    )
    response.status = 205
    return ""


@post('/cookies/<cookie_name>/unblock')
def post_unblock(cookie_name):
    query = """
        UPDATE pallets
        SET blocked = 0
        WHERE    1 = 1
        """
    params = []
    query += " AND cookie = ?"
    params.append(cookie_name)
    if request.query.after:
        query += " AND production_date < ?"
        params.append(unquote(request.query.after))
    if request.query.before:
        query += " AND production_date > ?"
        params.append(unquote(request.query.before))

    c = db.cursor()
    c.execute(
        query,
        params
    )
    response.status = 205
    return ""


run(host='127.0.0.1', port=8888)
