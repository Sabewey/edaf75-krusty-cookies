# EDAF75 Krusty Cookies

Developed by
- Lucas Boberg (lu3021bo-s)
- William Asplund (wi7574as-s)
- Erik Bergrahm (er4321be-s)

### UML Diagram
![UML Diagram](er-model.png "UML Diagram")

### Database Relations

- customers(<u>name</u>, address)
- pallets(<u>pallet_id</u>, production_date, blocked, location, _cookie_)
- cookies(<u>name</u>)
- material_amount(<u>_cookie_name_</u>, amount, <u>_material_</u>)
- raw_materials(<u>ingredient</u>, amount, unit, last_delivered_date, delivered_amount)

If we were to implment orders we would also have the following relations
- orders(<u>_order_id_</u>, delevery_date)
- order_amount(<u>_order_id_</u>, <u>_cookie_name_</u>, quantity)
### Create SQL Schema
[create-schema.sql](create-schema.sql)

Run the following to setup the sqlite database:
```bash
$ cat create-schema.sql | sqlite3 krusty.sqlite
```

### Setup server
Make sure to have Sqlite 3 and Python 3 installed with bottle and sqlite3 dependencies installed

Then run the command up abow to setup the sqlite database.

Then to run the program just run the following command:
```bash
$ python3 main.py
```

Then it should be available on http://127.0.0.1:8888